# Generated by Django 3.0.4 on 2020-04-21 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('secondapp', '0005_auto_20200420_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='register_table',
            name='about',
            field=models.TextField(blank=True, null=True),
        ),
    ]
